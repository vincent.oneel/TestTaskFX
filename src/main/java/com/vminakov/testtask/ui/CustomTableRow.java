package com.vminakov.testtask.ui;

import com.vminakov.testtask.data.TableRecord;
import javafx.scene.control.TableRow;

/**
 * Extends TableRow<T> for table row behavior customisation when it's item's internal counter reaches the value of 10
 *
 * @author Vladimir Minakov
 */

public class CustomTableRow extends TableRow<TableRecord> {
    @Override
    protected void updateItem(TableRecord item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null && !empty) {
            // in case item's internal counter reaches the value of 10 then filling background of the current row with
            // red
            if (item.getCounterValue() > 10){
                this.setStyle("-fx-background-color: red");
            } else {
                setStyle("");
            }
        }
    }
}
