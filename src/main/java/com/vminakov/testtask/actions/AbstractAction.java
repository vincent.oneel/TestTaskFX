package com.vminakov.testtask.actions;

import com.vminakov.testtask.data.TableRecord;
import javafx.collections.ObservableList;

/**
 * Base class for Action tasks
 */
public abstract class AbstractAction implements Runnable {
    protected ObservableList<TableRecord> recordsList;
    
    /**
     * Constructs the task object
     * @param recordsList Collection of objects that will be processed during the task/action execution
     */
    public AbstractAction(ObservableList<TableRecord> recordsList) {
        this.recordsList = recordsList;
    }
    
    /**
     * Constructs an empty task object
     */
    public AbstractAction() {
    
    }
}
