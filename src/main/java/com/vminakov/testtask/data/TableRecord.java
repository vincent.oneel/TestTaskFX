package com.vminakov.testtask.data;

import javafx.beans.property.SimpleStringProperty;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * TableRecord is a class which represents a data model for storing in a TableView UI component
 */

public class TableRecord {
    
    // programmer defined properties
    private SimpleStringProperty firstName;
    private SimpleStringProperty lastName;
    private SimpleStringProperty rule;
    private SimpleStringProperty counter;
    
    // internal object's counter
    private AtomicInteger counterValue = new AtomicInteger(1);
    
    /**
     * Constructs the data model object
     * @param firstName First name field of the table record object
     * @param lastName  Last name field of the table record object
     * @param rule Rule field of the table record object
     */
    public TableRecord(String firstName, String lastName, String rule) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.rule = new SimpleStringProperty(rule);
        this.counter = new SimpleStringProperty(String.format("c=%d", counterValue.get()));
    }
    
    /**
     * Getter method to retrieve the first name property value
     * @return the firstName property value
     */
    public String getFirstName() {
        return firstName.get();
    }
    
    /**
     * Getter method to retrieve the firstName property
     * @return firstName property
     */
    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }
    
    /**
     * Setter method for setting the firsName property value
     * @param firstName firsName property value
     */
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }
    
    /**
     * Getter method to retrieve the last name property value
     * @return the lastName property value
     */
    public String getLastName() {
        return lastName.get();
    }
    
    /**
     * Getter method to retrieve the lastName property
     * @return lastName property
     */
    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }
    
    /**
     * Setter method for setting the firsName property value
     * @param lastName lastName property value
     */
    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }
    
    /**
     * Getter method to retrieve the rule property value
     * @return the rule property value
     */
    public String getRule() {
        return rule.get();
    }
    
    /**
     * Getter method to retrieve the rule property
     * @return rule property
     */
    public SimpleStringProperty ruleProperty() {
        return rule;
    }
    
    /**
     * Setter method for setting the rule property value
     * @param rule rule property value
     */
    public void setRule(String rule) {
        this.rule.set(rule);
    }
    
    /**
     * Retrieves the internal counter property value
     * @return internal counter value
     */
    public String getCounter() {
        return counter.get();
    }
    
    /**
     * Retrieves internal counter property
     * @return counter property
     */
    public SimpleStringProperty counterProperty() {
        return counter;
    }
    
    /**
     * Sets the internal counter property value
     * @param counter counter value
     */
    public void setCounter(int counter) {
        this.counter.set(String.format("c=%d",counter));
        counterValue = new AtomicInteger(counter);
    }
    
    /**
     * Increases the internal counter value by 1 and sets the counter property value accordingly
     */
    public void inreaseCounterValue(){
        counter.set(String.format("c=%d", counterValue.incrementAndGet()));
    }
    
    /**
     * Retrieves the internal counter value
     * @return internal counter value as an int value
     */
    public int getCounterValue() {
        return counterValue.get();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        TableRecord that = (TableRecord) o;
        
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (!rule.equals(that.rule)) return false;
        return counter.equals(that.counter);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + rule.hashCode();
        result = 31 * result + counter.hashCode();
        return result;
    }
}
