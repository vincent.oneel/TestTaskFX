package com.vminakov.testtask;

import com.vminakov.testtask.core.Core;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The main Application's class. Prepares a window, parses UI from FXML file and displays it on a Scene inside
 *  of the window.
 *
 * @author Vladimir Minakov
 */

public class App extends Application {
    
    /**
     * Program entry point
     * @param args external arguments passed to the program when started
     */
    public static void main(String[] args) {
        launch();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void start(Stage window) throws Exception {
        window.setTitle("Test Task by Vladimir Minakov");
        window.setScene(new Scene(FXMLLoader.load(getClass().getResource("/views/MainScreenView.fxml")), 1024.0, 600.0));
        window.setOnCloseRequest(event -> Core.getInstance().getRunner().shutdown());
        window.show();
    }
}
