package com.vminakov.testtask.threading;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Running specified tasks in multiple threads.
 * @param <T> Type of the objects which can be processed as a task. All of types must implement Runnable.
 */
public class TaskRunner<T extends Runnable> {
    private ThreadPoolExecutor tasksExecutor;
    
    public TaskRunner() {
        tasksExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    }
    
    /**
     * Adds the task to executor
     * @param task task object which describes some actions which will be processed by thread available in thread pool
     */
    public void addTask(T task){
        tasksExecutor.execute(task);
    }
    
    /**
     * Shuts down ThreadPoolExecutor
     */
    public void shutdown(){
        if (tasksExecutor != null)
            tasksExecutor.shutdown();
    }
}