package com.vminakov.testtask.controller;

import com.vminakov.testtask.core.Core;
import com.vminakov.testtask.actions.AbstractAction;
import com.vminakov.testtask.data.TableRecord;
import com.vminakov.testtask.threading.TaskRunner;
import com.vminakov.testtask.ui.CustomTableRow;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Controller class for handling user actions from view
 */
public class MainScreenController implements Initializable {
    // id for customising records values
    private static final AtomicInteger ID = new AtomicInteger(1);
    
    // UI control. This field connected with the button which has the same id at FXML view file
    @FXML
    private Button btnAdd;
    // UI control. This field connected with the button which has the same id at FXML view file
    @FXML
    private Button btnModify;
    // UI control. This field connected with the button which has the same id at FXML view file
    @FXML
    private TableView<TableRecord> tableView;
    
    // Observable List of objects which placed into the table view
    private final ObservableList<TableRecord> tableRecords = FXCollections.observableArrayList();
    
    // Object that performs all task running in a threads
    private TaskRunner<AbstractAction> runner;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnAdd.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            onBtnAddClicked();
        });
        btnModify.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            onBtnModifyClicked();
        });
        initTableView();
        runner = new TaskRunner<>();
        Core.getInstance().setRunner(runner);
    }
    
    /**
     * method initializes the table view component
     */
    private void initTableView() {
        TableColumn<TableRecord, String> firstNameColumn = new TableColumn<>("first name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        TableColumn<TableRecord, String> lastNameColumn = new TableColumn<>("last name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        TableColumn<TableRecord, String> ruleColumn = new TableColumn<>("rule");
        ruleColumn.setCellValueFactory(new PropertyValueFactory<>("rule"));
        TableColumn<TableRecord, String> counterColumn = new TableColumn<>("counter");
        counterColumn.setCellValueFactory(new PropertyValueFactory<>("counter"));
        tableView.setRowFactory(p -> new CustomTableRow());
        tableView.getColumns().addAll(firstNameColumn, lastNameColumn, ruleColumn, counterColumn);
        tableView.setItems(tableRecords);
    }
    
    /**
     * Modify button handler method
     */
    private void onBtnModifyClicked() {
        runner.addTask(new AbstractAction(){
            @Override
            public void run() {
                updateRecordsList(() -> tableRecords.stream().forEach(TableRecord::inreaseCounterValue));
            }
        });
    }
    
    /**
     * Add button handler method
     */
    private void onBtnAddClicked() {
        runner.addTask(new AbstractAction() {
            private int actionRepeatCount = 3;
            @Override
            public void run() {
                updateRecordsList(() -> {
                    for (int i = 0; i < actionRepeatCount; i++){
                        int id = ID.getAndIncrement();
                        tableRecords.add(new TableRecord(String.format("John-%d", id), String.format("Smith-%d", id), "root"));
                    }
                });
            }
        });
    }
    
    /**
     * Updates tableRecords list elements with different runnable actions. Synchronises action's run method calls
     * because method called from multi thread context
     * @param action Runnable object whose run method will be executed.
     */
    private void updateRecordsList(Runnable action){
        System.out.println("meth called in " + Thread.currentThread().getName() + " thread context");
        synchronized (tableRecords){
            action.run();
            Platform.runLater(() -> tableView.refresh());
        }
    }
}