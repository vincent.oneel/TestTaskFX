package com.vminakov.testtask.core;

import com.vminakov.testtask.threading.TaskRunner;


/**
 * Singleton class for cross-context TaskRunner link store
 */
public class Core {
    private static volatile Core instance;
    
    private TaskRunner runner;
    
    private Core(){
    
    }
    
    public static Core getInstance() {
        if (instance == null){
            synchronized (Core.class){
                if (instance == null){
                    instance = new Core();
                }
            }
        }
        return instance;
    }
    
    public TaskRunner getRunner() {
        return runner;
    }
    
    public void setRunner(TaskRunner runner) {
        this.runner = runner;
    }
}
